# groceryapi

A webapp to serve as an API for `Project Pending`.

## Installing and running the application

* `auth_example.py` contains information about how to set up a connection to a database, edit this file with proper credentials and rename it to `auth.py`.
* `schema.sql` contains the database structure which can be set up on a local PostgreSQL server.
* Within the repository, run `pip install .`
* The application can be spun up by running `python -m groceryapi`

### Development installation

Follow the steps above, but instead of `pip install .`, run `pip install -e .`.
This creates a symlink to the repository and allows for changes to be made without having to reinstall the module.

## Updating the database

After installing, running `python update.py` will update the database.
The database will be rinsed of data before repopulating data from the webstores of Joker and Meny.
