--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: fuzzystrmatch; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;


--
-- Name: EXTENSION fuzzystrmatch; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION fuzzystrmatch IS 'determine similarities and distance between strings';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: env_code; Type: TABLE; Schema: public; Owner: cn
--

CREATE TABLE public.env_code (
    name character varying(40) NOT NULL
);


ALTER TABLE public.env_code OWNER TO cn;

--
-- Name: product; Type: TABLE; Schema: public; Owner: cn
--

CREATE TABLE public.product (
    id numeric(20,0) NOT NULL,
    store character varying(25) NOT NULL,
    title character varying(100) NOT NULL,
    subtitle character varying(100) NOT NULL,
    category character varying(100) NOT NULL,
    url character varying(255),
    image_url character varying(255),
    price_unit real NOT NULL,
    unit character varying(20) NOT NULL,
    weight real,
    is_new boolean NOT NULL,
    is_offer boolean NOT NULL
);


ALTER TABLE public.product OWNER TO cn;

--
-- Name: product_env_code; Type: TABLE; Schema: public; Owner: cn
--

CREATE TABLE public.product_env_code (
    product numeric(20,0) NOT NULL,
    store character varying(25) NOT NULL,
    name character varying(40) NOT NULL,
    code character varying(40)
);


ALTER TABLE public.product_env_code OWNER TO cn;

--
-- Name: product_promotion; Type: TABLE; Schema: public; Owner: cn
--

CREATE TABLE public.product_promotion (
    promotion numeric(20,0) NOT NULL,
    product numeric(20,0) NOT NULL,
    store character varying(25) NOT NULL
);


ALTER TABLE public.product_promotion OWNER TO cn;

--
-- Name: promotion; Type: TABLE; Schema: public; Owner: cn
--

CREATE TABLE public.promotion (
    id numeric(20,0) NOT NULL,
    name character varying(100),
    url character varying(100)
);


ALTER TABLE public.promotion OWNER TO cn;

--
-- Name: env_code env_code_pkey; Type: CONSTRAINT; Schema: public; Owner: cn
--

ALTER TABLE ONLY public.env_code
    ADD CONSTRAINT env_code_pkey PRIMARY KEY (name);


--
-- Name: product_env_code product_env_code_pkey; Type: CONSTRAINT; Schema: public; Owner: cn
--

ALTER TABLE ONLY public.product_env_code
    ADD CONSTRAINT product_env_code_pkey PRIMARY KEY (product, store, name);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: cn
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id, store);


--
-- Name: product_promotion product_promotion_pkey; Type: CONSTRAINT; Schema: public; Owner: cn
--

ALTER TABLE ONLY public.product_promotion
    ADD CONSTRAINT product_promotion_pkey PRIMARY KEY (promotion, product, store);


--
-- Name: promotion promotion_pkey; Type: CONSTRAINT; Schema: public; Owner: cn
--

ALTER TABLE ONLY public.promotion
    ADD CONSTRAINT promotion_pkey PRIMARY KEY (id);


--
-- Name: product_env_code product_env_code_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cn
--

ALTER TABLE ONLY public.product_env_code
    ADD CONSTRAINT product_env_code_name_fkey FOREIGN KEY (name) REFERENCES public.env_code(name);


--
-- Name: product_env_code product_env_code_product_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cn
--

ALTER TABLE ONLY public.product_env_code
    ADD CONSTRAINT product_env_code_product_fkey FOREIGN KEY (product, store) REFERENCES public.product(id, store);


--
-- Name: product_promotion product_promotion_product_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cn
--

ALTER TABLE ONLY public.product_promotion
    ADD CONSTRAINT product_promotion_product_fkey FOREIGN KEY (product, store) REFERENCES public.product(id, store);


--
-- Name: product_promotion product_promotion_promotion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cn
--

ALTER TABLE ONLY public.product_promotion
    ADD CONSTRAINT product_promotion_promotion_fkey FOREIGN KEY (promotion) REFERENCES public.promotion(id);


--
-- PostgreSQL database dump complete
--
