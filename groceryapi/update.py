import requests
from bs4 import BeautifulSoup as soup
import json
from groceryapi.models import (
    session,
    EnvironmentalCode,
    Promotion,
    Product,
    ProductEnvironmentalCode,
    ProductPromotion,
)
from pprint import pprint


def get_category(category):
    table = {
        "Drikkevarer": "Drikke",
        "Bakerivarer": "Bakeri",
        "Barneprodukter": "Barn",
        "Dyreprodukter": "Dyr",
        "Fisk/Skalldyr": "Fisk",
        "Frukt/Grønt": "Frukt & grønt",
        "Hus/Hjem Artikler": "Hus & hjem",
        "Kaker/Bakevarer": "Bakevarer og kjeks",
        "Meieriprodukter": "Meieri & egg",
        "Frokost/Pålegg": "Pålegg & frokost",
        "Personlige Artikler": "Personlige artikler",
        "Kioskvarer": "Snacks & godteri",
    }

    if category in table.keys():
        return table[category]
    else:
        return category


class MenyScraper:
    def __init__(self):
        self.db = session
        self.store = "Meny"
        self.base_url = "https://meny.no/WSPager?&page={page}&pageSize=200"
        self.current_page = 1
        self.get_items()

    def get_items(self):
        def insert(db, store, item):
            p = Product(
                item["id"],
                store,
                item["title"],
                item["subtitle"],
                get_category(item["categoryName"]),
                item["slugifiedUrl"],
                item["imageName"],
                item["pricePerUnit"],
                item["unit"],
                item["weight"],
                item["isNew"],
                item["isOffer"],
            )
            db.add(p)

            for env_code in item["environmentalCodes"]:
                if not db.query(EnvironmentalCode).get(env_code["name"]):
                    print(env_code["name"])
                    e = EnvironmentalCode(env_code["name"])
                    db.add(e)
                    db.commit()
                pe = ProductEnvironmentalCode(
                    item["id"], store, env_code["name"], env_code["code"]
                )
                db.add(pe)

            if item["promotionId"] and not db.query(Promotion).get(item["promotionId"]):
                pr = Promotion(item["promotionId"], item["promotionDisplayName"], None)
                db.add(pr)
                db.commit()

            if item["promotionId"]:
                ppr = ProductPromotion(item["promotionId"], item["id"], store)
                db.add(ppr)

            print("Inserted {} ({})".format(item["title"], item["subtitle"]))
            db.commit()

        while True:
            r = requests.get(self.base_url.format(page=self.current_page))

            self.current_page += 1
            if len(r.text) == 0:
                break

            meny_soup = soup(r.text, "lxml")
            item_soup = meny_soup.find("ul", class_="cw-products__list").find_all("li")
            for item in item_soup:
                item_data = item.find("div", attrs={"data-component": "AddToCart"})[
                    "data-prop-product"
                ]
                item_json = json.loads(item_data)
                insert(self.db, self.store, item_json)

    def __len__(self):
        return len(self.items)

    def __getitem__(self, key):
        return self.items[key]


class Item:
    def __init__(self, json):
        self.json = json
        self.ean = json["ean"]
        self.name = json["title"].replace("\n", "")
        self.subtitle = json["subtitle"]
        self.category = json["categoryName"]
        self.price = Price(json["pricePerUnit"], json["unit"])
        self.weight = json["weight"]

    def __repr__(self):
        return "<{}: {} - {} ({} kr/{})".format(
            self.__class__.__name__,
            self.name,
            self.subtitle,
            self.price.value,
            self.price.unit,
        )


class JokerScraper:
    def __init__(self):
        self.db = session
        self.store = "Joker"
        self.base_url = "https://nettbutikk.joker.no/api/products/search?page={page}&perpage=200&query="
        self.current_page = 1
        self.get_items()

    def get_items(self):
        def insert(db, store, item):
            p = Product(
                item["id"],
                store,
                item["title"],
                item["subtitle"],
                get_category(item["categoryname"]),
                item["slugifiedurl"],
                item["imagename"],
                item["priceperunit"],
                item["unit"],
                item["weight"],
                item["isnew"],
                item["isoffer"],
            )
            db.add(p)

            for env_code in item["icons"]:
                if not db.query(EnvironmentalCode).get(env_code["navn"]):
                    print(env_code["navn"])
                    e = EnvironmentalCode(env_code["navn"])
                    db.add(e)
                    db.commit()
                pe = ProductEnvironmentalCode(
                    item["id"], store, env_code["navn"], env_code["kode"]
                )
                db.add(pe)
                db.commit()

            for promo in item["promotions"]:
                if not db.query(Promotion).get(promo["id"]):
                    pr = Promotion(
                        promo["id"], promo["displayname"], promo["slugifiedurl"]
                    )
                    db.add(pr)
                    db.commit()
                ppr = ProductPromotion(promo["id"], item["id"], store)
                db.add(ppr)
                db.commit()

            print("Inserted {} ({})".format(item["title"], item["subtitle"]))

        while True:
            r = requests.get(self.base_url.format(page=self.current_page)).json()

            self.current_page += 1
            if len(r["products"]) == 0:
                break

            for item in r["products"]:
                # print(item["title"])
                insert(self.db, self.store, item)

    def __len__(self):
        return len(self.items)

    def __getitem__(self, key):
        return self.items[key]


def delete_data(sess):
    session.query(ProductEnvironmentalCode).delete()
    session.query(ProductPromotion).delete()
    session.query(Promotion).delete()
    session.query(EnvironmentalCode).delete()
    session.query(Product).delete()
    session.commit()


delete_data(session)
JokerScraper()
MenyScraper()
