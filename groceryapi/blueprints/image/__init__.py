from flask import Blueprint, request, jsonify
from groceryapi import basepath, build_path
from groceryapi.models import *
from groceryapi.helpers import results
from os import path
from groceryapi.blueprints.image.helpers import process_image, process_receipt

ALLOWED_EXTENSIONS = set(["png", "jpg", "jpeg"])

img = Blueprint("image", __name__, url_prefix="/image")

def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS

def process_categories(receipt):
    items = []
    for item in receipt:
        item = item.replace("'", "")
        products = (
            session.query(Product)
            .filter("concat(title, ' ', subtitle) % '{}'".format(item))
            .order_by(Product.category)
        )
        res = results(products)
        if len(res["meny"]) > 0:
            product = res["meny"][0]
        elif len(res["joker"]) > 0:
            product = res["joker"][0]
        else:
            product = {"price": 0.0, "category": "Unknown", "title": ""}
        items.append({"item": item, "actual_name": product["title"], "price": product["price"], "category": product["category"]})
    return items


@img.route("/", methods=["POST"])
def img_index():
    file = request.files["file"]
    if file and allowed_file(file.filename):
        image_text = process_image(file)
        receipt = process_receipt(image_text)
        categories = process_categories(receipt)
        return jsonify(categories)
    else:
        return jsonify([])

@img.route("/test/<file>")
def test_img(file):
    with open(build_path(basepath, "test", "{}.jpg".format(file)), 'rb') as f:
        image_text = process_image(f)
        receipt = process_receipt(image_text)
        categories = process_categories(receipt)
    return jsonify(categories)
