import pytesseract
import cv2
import numpy as np
from fuzzywuzzy import fuzz

def resize_image(im, new_width=900):
    h, w = im.shape[:2]
    r = new_width/w
    new_height = int(h*r)
    return cv2.resize(im, (new_width, new_height))

def process_joker(kvittering):
    varer = []
    should_print = False
    for line in kvittering:
        if len(line) == 0 or len(line.split()) == 0:
            continue
        if fuzz.ratio("Sum", line.split()[0]) > 80:
            break
        elif fuzz.ratio("SUM", line.split()[0]) > 80:
            break
        if should_print:
            if line[0] in ["*", "+"]:
                continue
            varer.append(line)
        if fuzz.ratio("Serienr", line[:7]) > 80:
            should_print = True
    return varer

def process_meny(kvittering):
    varer = []
    should_print = False
    for line in kvittering:
        if len(line) == 0 or len(line.split()) == 0:
            continue
        if fuzz.ratio("Sum", line.split()[0]) > 80:
            break
        if should_print:
            print(line)
            if line[0] in ["*", "+"]:
                continue
            varer.append(line)
        if fuzz.ratio("Oper", line[:4]) >= 75 or fuzz.ratio("Serie", line[:5]) >= 75:
            should_print = True
    return varer

def process_image(im):
    im_str = im.read()
    nparray = np.fromstring(im_str, np.uint8)
    image = cv2.imdecode(nparray, cv2.IMREAD_COLOR)
    image = resize_image(image)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 0, 200, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    blur = cv2.medianBlur(thresh, 1)

    text = pytesseract.image_to_string(blur, lang="nor", config="--psm {}".format(3))

    kvittering = text.split('\n')

    for line in kvittering:
        if line.startswith('JOKER'):
            return process_joker(kvittering)
        elif line.startswith('MENY'):
            return process_meny(kvittering)

def process_receipt(receipt):
    r = []
    for item in receipt:
        text = ""
        is_text = True
        for char in item:
            try:
                int(char)
                is_text = False
            except:
                if is_text:
                    text += char
        if len(text) > 0:
            if text.strip() != "SUBTOTAL":
                r.append(text.strip())
    return r
