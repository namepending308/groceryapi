from flask import Flask
from os import path

app = Flask(__name__)

basepath = path.dirname(path.realpath(__file__))
build_path = lambda *args: path.sep.join(args)

from groceryapi import views
