from flask import jsonify, request
from groceryapi import app
from groceryapi.models import *
from groceryapi.helpers import results
from groceryapi.blueprints.image import img

app.register_blueprint(img)

@app.route("/")
def index():
	return "hey"

@app.route("/query/")
def query():
    name = request.args.get("name", "")
    price = request.args.get("maxprice", 0)
    category = request.args.get("category", "")
    if len(name) == 0:
        return "please query with name :("
    try:
        price = float(price)
    except:
        return "please use a number for maxprice"
    products = (
        session.query(Product)
        .filter("concat(title, ' ', subtitle) % '{}'".format(name))
        .order_by(Product.category)
    )

    if price != 0.0:
        products = products.filter(Product.price_unit <= price)
    if len(category) != 0:
        products = products.filter(Product.category.ilike("%{}%".format(category)))
    return jsonify(results(products))


@app.route("/promotions/")
def promotions():
    products = (
        session.query(Product)
        .join(Product.promotions)
        .filter(Promotion.name.ilike("%tilbud%"))
        .order_by(Product.category)
    )
    return jsonify(results(products))
