from sqlalchemy import (
    Column,
    Integer,
    String,
    Numeric,
    Boolean,
    Float,
    ForeignKey,
    ForeignKeyConstraint,
    Table,
)
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from groceryapi import auth


Base = declarative_base()
engine = create_engine(
    "postgresql://{}:{}@{}/{}".format(auth.username, auth.password, auth.host, auth.db)
)


class EnvironmentalCode(Base):
    __tablename__ = "env_code"
    name = Column(String(40), primary_key=True)

    def __init__(self, name):
        self.name = name


class Promotion(Base):
    __tablename__ = "promotion"
    id = Column(Numeric(20), primary_key=True)
    name = Column(String(100))
    url = Column(String(100))

    products = relationship(
        "Product", secondary="product_promotion", back_populates="promotions"
    )

    def __init__(self, id, name, url):
        self.id = id
        self.name = name
        self.url = url


class Product(Base):
    __tablename__ = "product"
    id = Column(Numeric(20), primary_key=True)
    store = Column(String(25), primary_key=True)  # False = Meny, True = Joker
    title = Column(String(100), nullable=False)
    subtitle = Column(String(100), nullable=False)
    category = Column(String(100), nullable=False)
    url = Column(String(255))  # slugifiedurl / slugifiedUrl
    image_url = Column(String(255))  # imagename / imageName
    price_unit = Column(Float, nullable=False)
    unit = Column(String(20), nullable=False)
    weight = Column(Float)
    is_new = Column(Boolean, nullable=False)
    is_offer = Column(Boolean, nullable=False)

    promotions = relationship(
        "Promotion", secondary="product_promotion", back_populates="products"
    )

    def __init__(
        self,
        id,
        store,
        title,
        subtitle,
        category,
        url,
        image_url,
        price_unit,
        unit,
        weight,
        is_new,
        is_offer,
    ):
        self.id = id
        self.store = store
        self.title = title
        self.subtitle = subtitle
        self.category = category
        self.url = url
        self.image_url = image_url
        self.price_unit = price_unit
        self.unit = unit
        self.weight = weight
        self.is_new = is_new
        self.is_offer = is_offer


class ProductEnvironmentalCode(Base):
    __tablename__ = "product_env_code"
    __extend_existing__ = True
    product = Column(Numeric(20), primary_key=True)
    store = Column(String(25), primary_key=True)
    name = Column(String(40), primary_key=True)
    code = Column(String(40), nullable=True)

    __table_args__ = (
        ForeignKeyConstraint([product, store], [Product.id, Product.store]),
        ForeignKeyConstraint([name], [EnvironmentalCode.name]),
    )

    def __init__(self, product, store, name, code):
        self.product = product
        self.store = store
        self.name = name
        self.code = code


class ProductPromotion(Base):
    __tablename__ = "product_promotion"
    __extend_existing__ = True
    promotion = Column(Numeric(20), primary_key=True)
    product = Column(Numeric(20), primary_key=True)
    store = Column(String(25), primary_key=True)

    __table_args__ = (
        ForeignKeyConstraint([product, store], [Product.id, Product.store]),
        ForeignKeyConstraint([promotion], [Promotion.id]),
    )

    def __init__(self, promotion, product, store):
        self.promotion = promotion
        self.product = product
        self.store = store


Session = sessionmaker(bind=engine)
session = Session()
