from groceryapi.models import *
from flask import jsonify


def results(products):
    result = {"joker": [], "meny": []}

    base_url = {"joker": "https://nettbutikk.joker.no", "meny": ""}

    for product in products:
        promos = []
        promotions = (
            session.query(Promotion)
            .join(Promotion.products)
            .filter(
                (ProductPromotion.product == product.id)
                & (ProductPromotion.store == product.store)
                & (ProductPromotion.promotion == Promotion.id)
            )
        )
        for promo in promotions:
            promos.append(
                {
                    "id": int(promo.id),
                    "name": promo.name,
                    "url": "{}{}".format(base_url[product.store.lower()], promo.url),
                }
            )

        result[product.store.lower()].append(
            {
                "title": "{} {}".format(product.title, product.subtitle),
                "ean": int(product.id),
                "category": product.category,
                "price": product.price_unit,
                "weight": product.weight,
                "promotions": promos,
            }
        )
    return result
