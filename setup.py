from distutils.core import setup

setup(
    name='groceryapi',
    version='1.1',
    author='namepending308',
    author_email='kentsd16@student.uia.no',
    packages=['groceryapi'],
    install_requires=[
        'sqlalchemy',
        'psycopg2',
        'flask',
        'bs4',
        'requests',
        'opencv-python',
        'pytesseract',
        'numpy',
        'fuzzywuzzy'
    ]
)
